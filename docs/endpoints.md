### HOME PAGE

### Nav bar APIs -> This will be on every page

### Log in

-   Endpoint path: /token
-   Endpoint method: POST

-   Request shape (form):

    -   username: string
    -   password: string

-   Response: Account information and a token
-   Response shape (JSON):
    ```json
    {
      "account": {
        "gamertag": string,
        "picture_url": string,
        "bio": string,
      },
      "token": string
    }
    ```

### Log out

-   Endpoint path: /token
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: Always true
-   Response shape (JSON):
    ```json
    true
    ```

### (Search function) Get a list of Games

-   Endpoint path: api/games
-   Endpoint method: GET
-   Query parameters:

    -   q: the word(s) to search for

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of games
-   Response shape:
    ```json
    {
      "games": [
        {
          "title": string,
          "tags": string
        }
      ]
    }
    ```

### SEARCH GAMES PAGE

## Nav bar APIs

### Get a list of Games

-   Endpoint path: api/games
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of games
-   Response shape:
    ```json
    {
      "games": [
        {
          "id": int,
          "title": string,
          "image_url": string,
          "tags": string,
          "summary": string
        }
      ]
    }
    ```

### (Search function) Get a list of Games

-   Endpoint path: api/games
-   Endpoint method: GET
-   Query parameters:

    -   q: the word(s) to search for

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of games
-   Response shape:
    ```json
    {
      "games": [
        {
          "title": string,
          "id": int,
          "tags": string
        }
      ]
    }
    ```

### Filter list

-   Endpoint path: api/games
-   Endpoint method: GET
-   Request parameters:

    -   r: what is true for the checkboxes

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of games
-   Response shape:
    ```json
    {
      "games": [
        {
          "tags": string
        }
      ]
    }
    ```

### COMMUNITY PAGE

## Nav bar APIs

### Get a list of Posts

-   Endpoint path: api/posts
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of community posts
-   Response shape:

    ```json
    {
      "posts": [
        {
          "id": int,
          "user_id": int,
          "title": string,
          "author": string,
          "date/time": string,
          "description": string
        }
      ]
    }
    ```

### (Search function) Get a list of Games

-   Endpoint path: api/games
-   Endpoint method: GET
-   Query parameters:

    -   q: the word(s) to search for

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of games
-   Response shape:
    ```json
    {
      "games": [
        {
          "tags": string,
          "game_id": int
        }
      ]
    }
    ```

### Create a new Post

-   Endpoint path: api/posts
-   Endpoint method: POST

-   Headers:

    -   Authorization: Bearer token

-   Request body:

    ```json
    {
      "title": string,
      "date/time": string,
      "description": string,
      "user_id": int
    }
    ```

-   Response: An indication of success or failure
-   Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Edit/Update a Post

-   Endpoint path: api/posts
-   Endpoint method: PUT

-   Headers:

    -   Authorization: Bearer token

-   Request body:

    ```json
    {
      "title": string,
      "date/time": string,
      "description": string,
      "post_id": int
    }
    ```

-   Response: An indication of success or failure
-   Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Delete a post

-   Endpoint path: api/posts/{post_id}
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: Always true
-   Response shape (JSON):
    ```json
    true
    ```

### SPECIFIC POST PAGE

## Nav bar APIs

### Get a specific of Post

-   Endpoint path: api/posts/{post_id}
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of community posts
-   Response shape:

    ```json
    {
      "posts": [
        {
          "title": string,
          "post_id": int,
          "author": string,
          "date/time": string,
          "description": string,
          "comments": [
            {
            "comment_id": int,
            "description": string,
            "user_id": int
            }
          ]

        }
      ]
    }
    ```

### Create a new Post

-   Endpoint path: api/posts
-   Endpoint method: POST

-   Headers:

    -   Authorization: Bearer token

-   Request body:

    ```json
    {
      "title": string,
      "date/time": string,
      "user_id": int,
      "description": string
    }
    ```

-   Response: An indication of success or failure
-   Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Edit/Update a Post

-   Endpoint path: api/posts/{post_id}
-   Endpoint method: PUT

-   Headers:

    -   Authorization: Bearer token

-   Request body:

    ```json
    {
      "title": string,
      "date/time": string,
      "description": string
    }
    ```

-   Response: An indication of success or failure
-   Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Delete a post

-   Endpoint path: api/posts/{post_id}
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: Always true
-   Response shape (JSON):
    ```json
    true
    ```

### SPECIFIC USER PAGE

## Nav bar APIs

### Get user information

-   Endpoint path: api/user/{user_id}
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: A list of community posts
-   Response shape:

    ```json
    {
      "user":
        {
          "profile_pic": string,
          "gamer_tag": string,
          "bio": string,
          "rating": string,
          "reviews": string
        }
    }
    ```

### Create a new Review

-   Endpoint path: api/review/{user_id}
-   Endpoint method: POST

-   Headers:

    -   Authorization: Bearer token

-   Request body:

    ```json
    {
      "description": string,
      "rating": string
    }
    ```

-   Response: An indication of success or failure
-   Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Edit/Update a Review

-   Endpoint path: api/review/{review_id}
-   Endpoint method: PUT

-   Headers:

    -   Authorization: Bearer token

-   Request body:

    ```json
    {
      "description": string,
    }
    ```

-   Response: An indication of success or failure
-   Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Delete a review

-   Endpoint path: api/review/{review_id}
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: Always true
-   Response shape (JSON):
    ```json
    true
    ```
