from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import users, communities, posts, games
from authenticator import authenticator
import os

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(users.router)
app.include_router(communities.router)
app.include_router(posts.router)
app.include_router(games.router)

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "You hit the root path!"}
