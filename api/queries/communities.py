from pydantic import BaseModel
from typing import List
from queries.accounts import pool


class Error(BaseModel):
    message: str


class CommunityResponse(BaseModel):
    id: int
    title: str
    description: str


class CommunityCreate(BaseModel):
    title: str
    description: str


class CommunityDelete(BaseModel):
    id: int


class CommunityUpdate(BaseModel):
    title: str
    description: str


class CommunityQueries:
    def get_communities(self) -> List[CommunityResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, description
                        FROM communities
                        ORDER BY id
                        """
                    )
                    return [
                        self.record_to_community(record) for record in result
                    ]
        except Exception as e:
            print(e)
            raise Exception("Could not find any communities")

    def get(self, id: str):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, description
                        FROM communities
                        WHERE id = %s
                        """,
                        [id],
                    )
                    community = result.fetchone()
                    return CommunityResponse(
                        id=community[0],
                        title=community[1],
                        description=community[2],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not find community")

    def create_community(
        self, community: CommunityCreate
    ) -> List[CommunityResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO communities (title, description)
                        VALUES (%s, %s)
                        RETURNING id, title, description
                        """,
                        [community.title, community.description],
                    )
                    record = result.fetchone()
                    return self.record_to_community(record)
        except Exception as e:
            print(e)
            raise Exception("Could not create community")

    def record_to_community(self, record) -> CommunityResponse:
        return CommunityResponse(
            id=record[0], title=record[1], description=record[2]
        )

    def delete_community(self, community: CommunityDelete):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM communities
                        WHERE id = %s
                        """,
                        [community.id],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not delete community")

    def update_community(
        self, id: int, community: CommunityUpdate
    ) -> CommunityResponse:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE communities
                        SET title = %s, description = %s
                        WHERE id = %s
                        RETURNING id, title, description
                        """,
                        (community.title, community.description, id),
                    )
                    result = db.fetchone()
                    return self.record_to_community(result)
        except Exception as e:
            print(e)
            raise Exception("Could not update post")
