from pydantic import BaseModel
from typing import List
from queries.accounts import pool


class Error(BaseModel):
    message: str


class PostsResponse(BaseModel):
    id: int
    title: str
    content: str
    community_id: int


class PostsCreate(BaseModel):
    title: str
    content: str
    community_id: int


class PostsDelete(BaseModel):
    id: int


class PostsUpdate(BaseModel):
    title: str
    content: str


class PostsQueries:
    def get_posts(self) -> List[PostsResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, content, community_id
                        FROM posts
                        ORDER BY id
                        """
                    )
                    return [self.record_to_post(record) for record in result]
        except Exception as e:
            print(e)
            raise Exception("could not find any posts")

    def get(self, id: str):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, content, community_id
                        FROM posts
                        WHERE id = %s
                        """,
                        [id],
                    )
                    post = result.fetchone()
                    return PostsResponse(
                        id=post[0],
                        title=post[1],
                        content=post[2],
                        community_id=post[3],
                    )
        except Exception as e:
            print(e)
            raise Exception("could not find post")

    def create_post(self, post: PostsCreate) -> List[PostsResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO posts (title, content, community_id)
                        VALUES (%s, %s, %s)
                        RETURNING id, title, content, community_id;
                        """,
                        [post.title, post.content, post.community_id],
                    )
                    record = result.fetchone()
                    return self.record_to_post(record)
        except Exception as e:
            print(e)
            raise Exception("Could not create post")

    def record_to_post(self, record) -> PostsResponse:
        return PostsResponse(
            id=record[0],
            title=record[1],
            content=record[2],
            community_id=record[3],
        )

    def delete_post(self, post: PostsDelete):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM posts
                        WHERE id = %s
                        """,
                        [post.id],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not delete post")

    def update_post(self, id: int, post: PostsUpdate) -> PostsResponse:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE posts
                        SET title = %s, content = %s
                        WHERE id = %s
                        RETURNING id, title, content, community_id
                        """,
                        (post.title, post.content, id),
                    )
                    result = db.fetchone()
                    return self.record_to_post(result)
        except Exception as e:
            print(e)
            raise Exception("Could not update post")
