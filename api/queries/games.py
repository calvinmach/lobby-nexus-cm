from pydantic import BaseModel
from typing import List
from queries.accounts import pool


class GameUpdate(BaseModel):
    title: str
    tag_id: int
    description: str


class Error(Exception):
    message: str


class GameNotFoundError(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class GameResponse(BaseModel):
    id: int
    title: str
    tag: str
    description: str


class TagCreate(BaseModel):
    tag: str


class TagResponse(BaseModel):
    id: int
    tag: str


class GameCreate(BaseModel):
    title: str
    tag_id: int
    description: str


class GameDelete(BaseModel):
    id: int


class GameModel(BaseModel):
    id: int
    title: str
    tag_id: int
    description: str


class GameQueries:
    def get_games(self) -> List[GameResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, tag_id, description
                        FROM games
                        ORDER BY id
                        """
                    )
                    return [
                        self.record_to_game_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            raise Exception("Could not find any games")

    def get(self, id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, tag_id, description
                        FROM games
                        WHERE id = %s
                        """,
                        [id],
                    )
                    game = result.fetchone()
                    tag_id = game[2]
                    tag_name = self.get_tag_name(tag_id)
                    return GameResponse(
                        id=game[0],
                        title=game[1],
                        tag_id=tag_id,
                        tag=tag_name,
                        description=game[3],
                    )
        except Exception as e:
            print(e)
            raise Error("Could not find game")

    def get_tags(self) -> List[TagResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, tag
                        FROM tags
                        ORDER BY id
                        """
                    )
                    return [self.record_to_tag(record) for record in result]
        except Exception as e:
            print(e)
            raise Exception("Could not find any tags")

    def create_tag(self, tag: TagCreate) -> List[TagResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO tags (tag)
                        VALUES (%s)
                        RETURNING id, tag
                        """,
                        [tag.tag],
                    )
                    record = result.fetchone()
                    return self.record_to_tag(record)
        except Exception as e:
            print(e)
            raise Exception("Could not create tag")

    def record_to_tag(self, record) -> TagResponse:
        return TagResponse(
            id=record[0],
            tag=record[1],
        )

    def get_tag_name(self, tag_id: int) -> str:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT tag
                        FROM tags
                        WHERE id = %s
                        """,
                        [tag_id],
                    )
                    tag = result.fetchone()
                    if tag:
                        return tag[0]
                    else:
                        return "Tag not found"
        except Exception as e:
            print(e)
            return "Error cannot fetch tag name"

    def get_tag_names(self):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, tag
                        FROM tags
                        ORDER BY id
                        """
                    )
                    tags = [self.record_to_tag(record) for record in result]
                    tag_names = [tag.tag for tag in tags]
                    return tag_names
        except Exception as e:
            print(e)
            raise Exception("Could not find any tag names")

    def create_game(self, game: GameCreate) -> List[GameResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            INSERT INTO games (title, tag_id, description)
                            VALUES (%s, %s, %s)
                            RETURNING id, title, tag_id, description;
                            """,
                        [game.title, game.tag_id, game.description],
                    )
                    record = result.fetchone()
                    return self.record_to_game_out(record)
        except Exception as e:
            print(e)
            raise Exception("Could not create the game")

    def record_to_game_out(self, record) -> GameResponse:
        tag_id = record[2]
        tag_name = self.get_tag_name(tag_id)
        return GameResponse(
            id=record[0],
            title=record[1],
            tag=tag_name,
            description=record[3],
        )

    def delete_game(self, game: GameDelete):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM games
                        WHERE id = %s
                        """,
                        [game.id],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not delete the game")

    def update_game(self, id: int, game: GameUpdate) -> GameResponse:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE games
                        SET title = %s, tag_id = %s, description = %s
                        WHERE id = %s
                        RETURNING id, title, tag_id, description
                        """,
                        (game.title, game.tag_id, game.description, id),
                    )
                    result = db.fetchone()
                    return self.record_to_game_out(result)
        except Exception as e:
            print(e)
            raise Exception("Could not update the game")
