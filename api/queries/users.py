from pydantic import BaseModel
from typing import Union
from queries.accounts import pool


class Error(BaseModel):
    message: str


class UserResponse(BaseModel):
    id: int
    username: str
    email: str


class UserCreate(BaseModel):
    username: str
    password: str
    email: str


class ProfileDelete(BaseModel):
    id: int


class ProfileOut(BaseModel):
    id: int
    gamertag: str | None
    bio: str | None


class ProfileEdit(BaseModel):
    gamertag: str
    bio: str


class UserOutWithPassword(UserResponse):
    hashed_password: str


class UserQueries:
    def get_users(self) -> Union[UserResponse, UserOutWithPassword]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, email
                        FROM users
                        ORDER BY id
                        """
                    )
                    return [
                        self.record_to_user_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            raise Exception("Could not find any users")

    def get(self, username: str):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, email, password
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    user = result.fetchone()
                    return UserOutWithPassword(
                        id=user[0],
                        username=user[1],
                        email=user[2],
                        hashed_password=user[3],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not find user")

    def create_user(
        self, user: UserCreate, hashed_password: str
    ) -> Union[UserResponse, UserOutWithPassword]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users (username, password, email)
                        VALUES (%s, %s, %s)
                        RETURNING id, username, email;
                        """,
                        [user.username, hashed_password, user.email],
                    )
                    record = result.fetchone()
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            raise Exception("Could not create user")

    def record_to_user_out(self, record) -> UserResponse:
        return UserResponse(
            id=record[0],
            username=record[1],
            email=record[2],
        )

    def get_profile(self, id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, gamertag, bio
                        FROM users
                        WHERE id = %s
                        """,
                        [id],
                    )
                    profile = result.fetchone()
                    return ProfileOut(
                        id=profile[0],
                        username=profile[1],
                        gamertag=profile[2],
                        bio=profile[3],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not find user")

    def update_profile(self, id: int, users: ProfileEdit) -> ProfileOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE users
                        SET gamertag = %s, bio = %s
                        WHERE id = %s
                        RETURNING id, gamertag, bio
                        """,
                        [users.gamertag, users.bio, id],
                    )
                    record = result.fetchone()
                    return self.record_to_user_profile_out(record)
        except Exception as e:
            print(e)
            raise Exception("Could not update user")

    def delete_profile(self, users: ProfileDelete):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        WHERE id = %s
                        """,
                        [
                            users.id,
                        ],
                    )
        except Exception as e:
            print(e)
            raise Exception("Could not delete profile")

    def record_to_user_profile_out(self, record) -> ProfileOut:
        return ProfileOut(
            id=record[0],
            gamertag=record[1],
            bio=record[2],
        )
