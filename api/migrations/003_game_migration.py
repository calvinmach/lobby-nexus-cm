steps = [
    [
        """
        CREATE TABLE tags (
        id SERIAL PRIMARY KEY,
        tag VARCHAR(50) NOT NULL
        );
        """,
        """
        DROP TABLE tags;
        """
    ],
    [
        """
        CREATE TABLE games (
        id SERIAL PRIMARY KEY NOT NULL,
        title VARCHAR(100) NOT NULL,
        tag_id INT,
        description VARCHAR(500) NOT NULL,
        FOREIGN KEY (tag_id) REFERENCES tags(id)
        );
        """,
        """
        DROP TABLE games;
        """
    ]
]
