steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE communities (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            description TEXT NOT NULL
            -- FOREIGN KEY () REFERENCES (games)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE communities;
        """
    ],
    [
        """
        CREATE TABLE posts (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            content TEXT NOT NULL,
            community_id INT,
            FOREIGN KEY (community_id) REFERENCES communities(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE posts;
        """
    ]
]
