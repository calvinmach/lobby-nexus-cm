steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(100) UNIQUE NOT NULL,
            email VARCHAR(100) NOT NULL,
            password VARCHAR(100) NOT NULL,
            gamertag VARCHAR(100),
            profile_pic TEXT,
            bio TEXT,
            rating INT CHECK (rating >= 0 AND rating <= 5),
            reviews VARCHAR(100)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ]
]
