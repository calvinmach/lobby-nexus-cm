from queries.posts import PostsQueries, PostsResponse
from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator


client = TestClient(app)


class fake_posts_query:
    def get_posts(self):
        return [
            PostsResponse(
                id=1,
                title="test",
                content="testcontent",
                community_id=1,
            ),
            PostsResponse(
                id=2,
                title="testing",
                content="supertestcontent",
                community_id=2,
            ),
        ]


def generated_user():
    return {
        "id": 1,
        "username": "username",
        "email": "email@gmail.com",
        "hashed_password": "ar2opfkoc1a",
    }


def test_posts_list():
    app.dependency_overrides[PostsQueries] = fake_posts_query
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = generated_user
    response = client.get("/posts")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [
        PostsResponse(
            id=1, title="test", content="testcontent", community_id=1
        ),
        PostsResponse(
            id=2, title="testing", content="supertestcontent", community_id=2
        ),
    ]
