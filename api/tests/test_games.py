from queries.games import GameQueries, GameResponse, TagResponse
from fastapi.testclient import TestClient
from main import app


client = TestClient(app)


class fake_games_query:
    def get_games(self):
        return [
            GameResponse(
                id=1,
                title="Minecraft",
                tag="Adventure",
                description="Mining away",
            ),
            GameResponse(
                id=2,
                title="Driving Simulator",
                tag="Race",
                description="Drive safe",
            ),
        ]


def test_games_list():
    app.dependency_overrides[GameQueries] = fake_games_query

    response = client.get("/games")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [
        GameResponse(
            id=1, title="Minecraft", tag="Adventure", description="Mining away"
        ),
        GameResponse(
            id=2,
            title="Driving Simulator",
            tag="Race",
            description="Drive safe",
        ),
    ]
