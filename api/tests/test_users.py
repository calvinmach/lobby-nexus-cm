from queries.users import UserQueries, UserResponse
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


class fake_users_query():
    def get_users(self):
        return [UserResponse(
            id=1,
            username="test",
            email="test@test.com"
        ), UserResponse(
            id=2,
            username="jordan",
            email="cool@awesome.com"
        )]


def test_users_list():
    app.dependency_overrides[UserQueries] = fake_users_query

    response = client.get("/users")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [UserResponse(
            id=1,
            username="test",
            email="test@test.com"
        ), UserResponse(
            id=2,
            username="jordan",
            email="cool@awesome.com"
        )]
