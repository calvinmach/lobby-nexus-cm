from main import app
from fastapi.testclient import TestClient
from queries.communities import CommunityQueries, CommunityResponse
from authenticator import authenticator

client = TestClient(app)


class FakeQuery():
    def get_communities(self):
        return [
            CommunityResponse(
                id=1,
                title="Test for Tests",
                description="Test to see if test is testing"
            )
        ]

def get_mock_user():
    return {
        "id":1,
        "username": "Jericho",
        "email": "test@test.com",
        "hashed_password": "ghghghgh",}


def test_communities_list():
    app.dependency_overrides[CommunityQueries] = FakeQuery
    app.dependency_overrides[authenticator.get_current_account_data] = get_mock_user

    response = client.get("/communities")
    app.dependency_overrides = {}

    assert response.status_code == 200
    sample_data = [
        CommunityResponse(
            id=1,
            title="Test for Tests",
            description="Test to see if test is testing"
        )
    ]
    assert response.json() == sample_data