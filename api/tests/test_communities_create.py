from main import app
from fastapi.testclient import TestClient
from queries.communities import (
    CommunityQueries,
    CommunityResponse,
    CommunityCreate,
)
from authenticator import authenticator
client = TestClient(app)


class FakeQueries:
    def create_community(self, info: CommunityCreate):
        return CommunityResponse(
            id=1, title=info.title, description=info.description
        )


def get_mock_user():
    return {
        "id": 1,
        "username": "User",
        "email": "User@test.com",
        "hashed_password": "userpassword",
    }


def test_create_community():
    app.dependency_overrides[CommunityQueries] = FakeQueries
    app.dependency_overrides[authenticator.get_current_account_data] = get_mock_user
    data = {
        "title": "Test Community",
        "description": "This is a test community.",
    }
    response = client.post("/communities", json=data)
    app.dependency_overrides = {}

    assert response.status_code == 200

    sample_data = CommunityResponse(
        id=1, title=data["title"], description=data["description"]
    )
    assert response.json() == sample_data
