from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from pydantic import BaseModel
from typing import List, Union
from queries.posts import (
    PostsQueries,
    PostsResponse,
    PostsCreate,
    PostsDelete,
    PostsUpdate,
    Error,
)
from authenticator import authenticator


class PostsForm(BaseModel):
    title: str
    content: str


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/posts", response_model=List[PostsResponse])
def posts_list(
    repo: PostsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        posts = repo.get_posts()
        return posts
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while fetching posts",
        )


@router.post("/posts", response_model=Union[PostsResponse, HttpError])
async def create_post(
    info: PostsCreate,
    request: Request,
    response: Response,
    posts: PostsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        post = posts.create_post(info)
        return post
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="cannot create a post with those details",
        )


@router.get("/posts/{id}", response_model=PostsResponse)
def get_a_post(
    id: int,
    repo: PostsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        post = repo.get(id)
        if post:
            return post
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="No Existing Post",
            )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred when fetching post",
        )


@router.delete("/posts/{id}")
def delete_a_post(
    id: int,
    repo: PostsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        delete_post = PostsDelete(id=id)
        repo.delete_post(delete_post)
        return {"message": "Post has been deleted"}
    except Exception as e:
        print(e)
        raise Exception("Could not delete post")


@router.put("/posts/{id}", response_model=PostsResponse)
def update_post(
    id: int,
    update_post: PostsUpdate,
    repo: PostsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        update_post = repo.update_post(id, update_post)
        return update_post
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred when updating the post",
        )
