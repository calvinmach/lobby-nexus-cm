from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from pydantic import BaseModel
from typing import List, Union
from queries.communities import (
    CommunityQueries,
    CommunityResponse,
    CommunityCreate,
    CommunityDelete,
    CommunityUpdate,
    Error,
)
from authenticator import authenticator


class CommunityForm(BaseModel):
    title: str
    description: str


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/communities", response_model=List[CommunityResponse])
def communities_list(
    repo: CommunityQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        communities = repo.get_communities()
        return communities
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while fetching communities",
        )


@router.post(
    "/communities", response_model=Union[CommunityResponse, HttpError]
)
async def create_community(
    info: CommunityCreate,
    request: Request,
    response: Response,
    communities: CommunityQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        community = communities.create_community(info)
        return community
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a community with those details",
        )


@router.get("/communities/{id}", response_model=CommunityResponse)
def get_a_community(
    id: int,
    repo: CommunityQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        community = repo.get(id)
        if community:
            return community
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="No Existing Community",
            )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred when fetching community",
        )


@router.delete("/communities/{id}")
def delete_a_community(
    id: int,
    repo: CommunityQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        delete_community = CommunityDelete(id=id)
        repo.delete_community(delete_community)
        return {"message": "Community has been nuked"}
    except Exception as e:
        print(e)
        raise Exception("Community could not be nuked")


@router.put("/communities/{id}", response_model=CommunityResponse)
def update_community(
    id: int,
    update_community: CommunityUpdate,
    repo: CommunityQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        update_community = repo.update_community(id, update_community)
        return update_community
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred when updating the community",
        )
