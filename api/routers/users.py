from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel
from queries.users import (
    UserQueries,
    UserResponse,
    UserCreate,
    ProfileEdit,
    ProfileOut,
    ProfileDelete,
    Error,
)


class AccountForm(BaseModel):
    username: str
    password: str


class ProfileForm(BaseModel):
    gamertag: str
    bio: str


class AccountToken(Token):
    account: UserResponse


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/api/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: UserResponse = Depends(
        authenticator.try_get_current_account_data
    ),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/users", response_model=AccountToken | HttpError)
async def create_account(
    info: UserCreate,
    request: Request,
    response: Response,
    accounts: UserQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create_user(info, hashed_password)
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.get("/users/profile/{id}", response_model=ProfileOut)
def user_profile(id: int, repo: UserQueries = Depends()):
    try:
        profile = repo.get_profile(id)
        if profile:
            return profile
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Profile not found",
            )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while fetching profile details.",
        )


@router.put("/users/profile/{id}", response_model=ProfileOut)
async def update_profile(
    id: int, profile_update: ProfileEdit, repo: UserQueries = Depends()
):
    try:
        updated_profile = repo.update_profile(id, profile_update)
        return updated_profile
    except Error as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e),
        )


@router.delete("/users/profile/{id}")
async def delete_profile(id: int, repo: UserQueries = Depends()):
    profile_delete = ProfileDelete(id=id)
    try:
        repo.delete_profile(profile_delete)
        return {"message": "Profile deleted"}
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while deleting the profile.",
        )


@router.get("/users", response_model=list[UserResponse])
def users_list(repo: UserQueries = Depends()):
    try:
        users = repo.get_users()
        return users
    except Exception as e:
        return Error(message=str(e))
