from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from pydantic import BaseModel
from queries.games import (
    GameResponse,
    GameCreate,
    GameQueries,
    TagCreate,
    TagResponse,
    Error,
    GameNotFoundError,
    GameDelete,
    GameUpdate,
)
from typing import List, Union


class GamesForm(BaseModel):
    title: str
    tag_id: int
    description: str


class TagsForm(BaseModel):
    tag: str


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/games", response_model=List[GameResponse])
def games_list(repo: GameQueries = Depends()):
    try:
        games = repo.get_games()
        return games
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while fetching games:" + str(e),
        )


@router.post("/games", response_model=Union[GameResponse, HttpError])
async def create_game(
    info: GameCreate,
    request: Request,
    response: Response,
    games: GameQueries = Depends(),
):
    try:
        game = games.create_game(info)
        return game
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a game. Invalid details.",
        )


@router.post("/tag", response_model=Union[TagResponse, HttpError])
async def create_tag(
    info: TagCreate,
    request: Request,
    response: Response,
    tags: GameQueries = Depends(),
):
    try:
        tag = tags.create_tag(info)
        return tag
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a tag. Invalid details.",
        )


@router.get("/tags", response_model=List[TagResponse])
def tags_list(repo: GameQueries = Depends()):
    try:
        tags = repo.get_tags()
        return tags
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while fetching tags.",
        )


@router.get("/games/{id}", response_model=GameResponse)
def get_game(id: int, repo: GameQueries = Depends()):
    try:
        game = repo.get(id)
        if game:
            return game
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Game not found",
            )
    except GameNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e),
        )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while fetching game details.",
        )


@router.delete("/games/{id}")
def delete_game(id: int, repo: GameQueries = Depends()):
    game_delete = GameDelete(id=id)
    try:
        repo.delete_game(game_delete)
        return {"message": "Game deleted"}
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while deleting the game.",
        )


@router.put("/games/{id}", response_model=GameResponse)
def update_game(
    id: int, game_update: GameUpdate, repo: GameQueries = Depends()
):
    try:
        updated_game = repo.update_game(id, game_update)
        return updated_game
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred when updating the game",
        )
