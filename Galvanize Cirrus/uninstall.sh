#!/bin/bash
# 2023 Galvanize, All rights reserved.

if [[ -f /usr/local/bin/glv-cloud-cli ]]; then
rm /usr/local/bin/glv-cloud-cli
fi

if [[ -f /usr/local/glv-cloud-cli ]]; then
rm -rf /usr/local/glv-cloud-cli
fi

fingerprint=$(openssl x509 -noout -fingerprint -sha1 -C -inform pem -in \
 /usr/local/galvanize/localhost.crt | grep Fingerprint | sed 's/^.*=//' | \
 sed 's/://g')

sudo security delete-certificate -Z ${fingerprint} /Library/Keychains/System.keychain
