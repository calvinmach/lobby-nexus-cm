#!/bin/bash
# 2023 Galvanize, All rights reserved.

# Make install directory
mkdir -p /usr/local/galvanize

# Make sure bin exists even though macOS already adds it to path... weird
mkdir -p /usr/local/bin

# Install cli binary
sudo cp glv-cloud-cli /usr/local/bin/glv-cloud-cli
