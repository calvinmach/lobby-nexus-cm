## Monday 9/18:

​
Was assigned to my group and it looks like it's gonna be a great project. Came up with the name for our group and our project.
​

## Tuesday 9/19:

​
Started work on our Wireframe and our Code of Conduct documentation. The ideas really started to come alive as we were making the wireframe. Turns out it was a lot of fun to collaborate with my group on how we want the website to look/function.
​

## Wednesday 9/20:

​
Created the GitLab group project repo and added everyone to it. Got all of our Docker Containers working on our respective computers with the cloned repo in each. Added more to the Wireframe excalidraw.
​

## Thursday 9/21:

​
Started working on the API Endpoints and the MVP. API Endpoints turned out to be tricker than expected and needed more time with it. Finalized the Wireframe and got it to a point where we are all happy with it.
​

## Friday 9/22:

​
Finished the API Endpoints and MVP after a discussion with Jordan about how we can organize our SQL database design and the best practice for commits/pushes. Started working on the idea for our User Stories and some extra ideas like how we want the color scheme to look and the overall design of the buttons/website.
​

## Monday 9/25:

​
Presented our project to the class and instructors. Got valuable feedback on what parts of our project we should push to strech goals and what parts we need to focus on. We got tips and feedback on how we should set up our APIs and our Databases. Looking forward to starting to write some code.
​

## Tuesday 9/26:

​
Got our Docker containers running as a group. Started to work on the Database Diagram to attach to the group notion. Went over our gitlab set up to make sure that we're all added and ready to merge our local repositories with the main remote repository.
​

## Wednesday 9/27:

​
Today we all decided that we are going to use SQL as our database so we started building our migration for it. We created the users and the users_profile tables, updated the docker.yaml file to handle using SQL across all of our local libraries, and got the tables to show up in Beekeeper.
​

## Thursday 9/28:

​
Today we got our API POST endpoint to work and save the data to the localhost:8000/docs# backend. Super gratifying seeing that working because we were all spinning our wheels about it in the morning. Made an Issue/Merge Request for the API POST since we didn't make one in the morning. Had to have Jordan come in and explain the proper way to make Issues and Migrations moving forward.
​

## Monday 10/9:

​
Today I started researching on how to do backend authentication. Seems like a complicated issue but I've started the process and I'll be doing my best over the weekend to try to understand what's happening.
​

## Tuesday 10/10:

​
Today I continued to work on the backend authentication. I feel like I ended up getting it to work but I'm having issues getting the token for the bearer so I'll need to look into that tomorrow.
​

## Wednesday 10/11:

​
Today I FINALLY finished the backend authentication. It was a bit of a nightmare to get to work but I can finally get the token for the bearer so everytime someone logs in, it is protected and you can get the token until they log out, then it is deleted.
​

## Thursday 10/12:

​
Today I started to work on creating the backend for the user profile. I made a profile.py page for the user to update their profile but it's currently not working. Going to try to hammer out the details tomorrow.
​

## Friday 10/13:

​
Today I spent most of my day helping out everyone else in the group, I didn't end up doing my work on my own endpoint because I wanted to help everyone else instead. Teamwork makes the dream work.
​

## Monday 10/16:

​
Today I fought with my backend profile page some more. For some reason I can't seem to fetch the data from my profile table. I can get the data from my Users table just fine but I think it might be an issue with my foreign key and how I'm referencing each table.
​

## Tuesday 10/17:

​
Today I got the backend profile working after some tinkering with the code. I was having an issue displaying what fields I wanted to display and I wasn't able to have certain fields (gamertag, bio) autopopulate on the backend. I had set them to DEFAULT NULL prior to today but that wasn't working. By removing that, I was able to just pull a null value from the start so the user has the ability to edit that data in stead of trying to creating it separately.
​

## Wednesday 10/18:

​
Today I finally got the backend User Profile to work correctly! It was a little disheartening to hear that it doesn't count for much of the grade since it was an endpoint based around authentication and that was never explained to us. If the issue is brought up I'm going to discuss that because I feel that is unfair. At least by doing a PUT request, I hit the requirements for the individual backend endpoint.
​

## Thursday 10/19:

​
Today I added Bootstrap script into our project so that we can have some styling on our Frontend. I was attempting to do Tailwind but I couldn't get it to work and we're starting to run short on time so I'm going to save that for the final week.
​

## Friday 10/20:

​
Started writing my Unit Test for User Profile. I want to be able to POST a user through a Unit Test but I'm not exactly sure how to write a Unit Test. Feels weird to attempt to write a test about soemthing I'm assuming is going to happen.
​

## Monday 10/23:

​
Today I fixed my Unit Test for User Profile. I kept getting a 422 error before and I wasn't sure what was going on. After researching what a 422 means I ended up figuring out that I was attempting to get data from a table that didn't exist anymore.
​

## Tuesday 10/24:

​
Today Jordan suggested to me that I combine my backend for my Users table and my Profile table. The issues I was having were revolving around trying to fetch similar data from two different tables and React having an issue getting pulling from nested tables.
​

## Wednesday 10/25:

​
Today I started the final pass on my User Profile page in order to update the gamertag and bio lines on the backend. Running into an issue fetching the data and attempting to put save/edit on a single button. Can't seem to get it to work so I'm going to reach out to Jordan in order to try to fix it.
​

## Thursday 10/26:

​
Today I finished my frontend Update for the User Profile. The issue I was running into before was the information was not getting saved as I wasn't fetching data from the proper source.
​

## Friday 10/27:

​
Today we are doing our presentation. We're spending the morning cleaning up the code so that we can present our project with no issues.
