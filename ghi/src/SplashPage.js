import useToken from "@galvanize-inc/jwtdown-for-react";

function SplashPage() {
	const { token } = useToken('')

	if(token){
		return (
			<>
			<div className="px-5 py-5 my-5 text-center">
				<h1 className="display-5 fw-bold">Lobby Nexus</h1>
				<div className="col-lg-6 mx-auto">
					<p className="lead mb-4">join dozens of gamer seeking gamers</p>
					<h2>About us:</h2>
					<p>
						We are a small group of four Hack Reactor graduates brought
						together by a love of gaming. If you find it hard to find
						and interact with communities, this is the place for you.
					</p>
				</div>
			</div>
			</>
			);
	} else {
		return(
		<>
		<h1>PLEASE SIGN UP OR SIGN IN TO CONTINUE</h1>
		</>
		)
	}
}

export default SplashPage;
