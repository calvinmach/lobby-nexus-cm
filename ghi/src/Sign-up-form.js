import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function SignUpForm() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { register } = useToken();
  const navigate = useNavigate();
  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const data = {
      username: username,
      email: email,
      password: password
    };

    register(data, `${process.env.REACT_APP_API_HOST}/users`);
    event.target.reset();
    navigate("/");
  };

  return (
    <>
      <div>
        <div>
          <div>
            <h1>Create an account</h1>
            <form onSubmit={handleSubmit} id="create-account">
              <div>
                <input
                  onChange={handleUsernameChange}
                  value={username}
                  placeholder="Username"
                  required
                  type="text"
                  name="username"
                  id="username"
                />
                <label htmlFor="username">Username</label>
              </div>
              <div>
                <input
                  onChange={handleEmailChange}
                  value={email}
                  placeholder="Email"
                  required
                  type="text"
                  name="email"
                  id="email"
                />
                <label htmlFor="email">Email</label>
              </div>
              <div>
                <input
                  onChange={handlePasswordChange}
                  value={password}
                  placeholder="Password"
                  required
                  type="text"
                  name="password"
                  id="password"
                />
                <label htmlFor="password">Password</label>
              </div>
              <button>Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default SignUpForm;
