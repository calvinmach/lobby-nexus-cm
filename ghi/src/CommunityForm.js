import { useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

function CommunityForm(){
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const { token } = useToken('')

    const navigate = useNavigate();

    const handleTitleChange = (event) =>{
        setTitle(event.target.value)
    }

    const handleDescriptionChange = (event) =>{
        setDescription(event.target.value)
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data={
            title: title,
            description: description,
        };

        const communityUrl = `${process.env.REACT_APP_API_HOST}/communities`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: "include",
        };

        const response = await fetch(communityUrl, fetchConfig);
        if( response.ok){
            setTitle('')
            setDescription('')
            navigate('/communities/view')
        }
    }
    if(token){
    return(
        <>
        <div>
            <h1>Add a Community</h1>
            <form onSubmit={handleSubmit} id="create-community">
                <div>
                    <input onChange={handleTitleChange} value={title} placeholder="Title"
                    required type="text" name="Title" id="Title" />
                    <label htmlFor="Title">Title</label>
                </div>
                <div>
                    <textarea onChange={handleDescriptionChange} value={description} placeholder="Description"
                    name='desc' id="desc"/>
                    <label htmlFor="desc">Description</label>
                </div>
                <button>Create</button>
            </form>
        </div>
        </>
    )
    } else {
        return(
		<>
		<h1>PLEASE SIGN UP OR SIGN IN TO CONTINUE</h1>
		</>
		)
    }

}
export default CommunityForm
