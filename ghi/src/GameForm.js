import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function GameForm() {
  const [title, setTitle] = useState("");
  const [tagId, setTagId] = useState("");
  const [description, setDescription] = useState("");
  const [tags, setTags] = useState([]);
  const navigate = useNavigate();

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const handleTagIdChange = (event) => {
    setTagId(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      title: title,
      tag_id: tagId,
      description: description
    };

    const url = `${process.env.REACT_APP_API_HOST}/games`;
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      if (response.ok) {
        navigate("/games");
      }
    } catch (error) {
      console.error("Error creating the game:", error);
    }
  };

  useEffect(() => {
    const fetchTags = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/tags`;
      const response = await fetch(url);
      if (response.ok) {
        const tagData = await response.json();
        setTags(tagData);
      }
    };

    fetchTags();
  }, []);

  return (
    <>
      <div>
        <div>
          <div>
            <h1>Create a game</h1>
            <form onSubmit={handleSubmit} id="title">
              <div>
                <input
                  onChange={handleTitleChange}
                  value={title}
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                />
                <label htmlFor="title">Title</label>
              </div>
              <div>
                <select onChange={handleTagIdChange} value={tagId}>
                  <option value="">Select a Tag</option>
                  {tags.map((tag) => (
                    <option key={tag.id} value={tag.id}>
                      {tag.tag}
                    </option>
                  ))}
                </select>
                <label htmlFor="tagId">Tags</label>
              </div>
              <div>
                <textarea
                  onChange={handleDescriptionChange}
                  value={description}
                  placeholder="Description"
                  required
                  type="text"
                  name="description"
                  id="description"
                />
                <label htmlFor="description">Description</label>
              </div>
              <button>Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default GameForm;
