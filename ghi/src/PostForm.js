import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

function PostForm() {
  const [communities, setCommunities] = useState([]);
  const [community, setCommunity] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const { token } = useToken("");

  const navigate = useNavigate();

  const handleCommunityChange = (event) => {
    const value = event.target.value;
    setCommunity(value);
  };
  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  };
  const handleContentChange = (event) => {
    const value = event.target.value;
    setContent(value);
  };

  const getData = async () => {
    const Url = `${process.env.REACT_APP_API_HOST}/communities`;
    const response = await fetch(Url, { credentials: "include" });
    if (response.ok) {
      const data = await response.json();
      if (data) {
        setCommunities(data);
      }
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      title: title,
      content: content,
      community_id: community
    };

    const postUrl = `${process.env.REACT_APP_API_HOST}/posts`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json"
      },
      credentials: "include"
    };
    const response = await fetch(postUrl, fetchConfig);
    if (response.ok) {
      setTitle("");
      setContent("");
      setCommunity("");
      navigate("/posts");
    }
  };

  if (token) {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create New Post</h1>
              <form onSubmit={handleSubmit} id="create-post-form">
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    name="title"
                    value={title}
                    onChange={handleTitleChange}
                    placeholder="Title"
                    id="title"
                    className="form-control"
                  />
                  <label htmlFor="title">Title</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea
                    name="content"
                    value={content}
                    onChange={handleContentChange}
                    placeholder="Content"
                    id="content"
                    className="form-control"
                  />
                  <label htmlFor="content">Content</label>
                </div>
                <div className="mb-3">
                  <select
                    onChange={handleCommunityChange}
                    value={community}
                    required
                    id="community"
                    name="community"
                    className="form-select"
                  >
                    <option value="">Choose a Community</option>
                    {communities.map((community) => {
                      return (
                        <option key={community.id} value={community.id}>
                          {community.title}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create Post</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <>
        <h1>Please Log In or Sign Up</h1>;
      </>
    );
  }
}

export default PostForm;
