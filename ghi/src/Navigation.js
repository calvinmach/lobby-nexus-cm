import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
function Nav() {
  const { logout } = useToken();

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="">
            Lobby Nexus
          </NavLink>
          <NavLink to="/sign-up">Sign up</NavLink>
          <NavLink to="/login">Login</NavLink>
          <NavLink to="/communities/view">Communities</NavLink>
          <NavLink to="/games">Games</NavLink>
          <NavLink to="/users/profile">Profile</NavLink>
          <NavLink to="/posts">Posts</NavLink>
          <button onClick={logout}>Logout</button>
        </div>
      </nav>
    </>
  );
}
export default Nav;
