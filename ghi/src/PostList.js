import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

function PostList() {
  const [posts, setPosts] = useState([]);
  const [communities, setCommunities] = useState([]);
  const [search, setSearch] = useState("");
  const { token } = useToken("");

  const getData = async () => {
    Promise.all([
      fetch(`${process.env.REACT_APP_API_HOST}/communities`, {
        credentials: "include"
      }),
      fetch(`${process.env.REACT_APP_API_HOST}/posts`, {
        credentials: "include"
      })
    ])
      .then(([getCommunities, getPosts]) =>
        Promise.all([getCommunities.json(), getPosts.json()])
      )
      .then(([dataCommunities, dataPosts]) => {
        setCommunities(dataCommunities);
        setPosts(dataPosts);
      });
  };
  const handleDelete = async (postId) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/posts/${postId}`,
      {
        method: "DELETE",
        credentials: "include"
      }
    );
    if (response.ok) {
      getData();
    } else {
      console.error("Could not delete");
    }
  };
  const handleSearchChange = (event) => {
    const value = event.target.value;
    setSearch(value);
  };
  useEffect(() => {
    getData();
  }, []);

  if (token) {
    return (
      <>
        <NavLink to="/posts/create">Create a Post</NavLink>
        <div>
          <select
            onChange={handleSearchChange}
            value={search}
            required
            id="community"
            name="community"
            className="form-select"
          >
            <option value="">All Communities</option>
            {communities.map((community) => {
              return (
                <option key={community.id} value={community.id}>
                  {community.title}
                </option>
              );
            })}
          </select>
          <h1>Post List</h1>
          <ul>
            {posts
              .filter((post) => {
                if (search === "") {
                  return true;
                } else {
                  return post.community_id === parseInt(search);
                }
              })
              .map((post) => (
                <li key={post.id}>
                  <h2>{post.title}</h2>
                  <p>{post.content}</p>
                  <p>
                    {
                      communities.find(
                        (community) => community.id === post.community_id
                      ).title
                    }
                  </p>
                  <p>
                    <button
                      className="btn btn-info"
                      onClick={() => handleDelete(post.id)}
                    >
                      Delete
                    </button>
                  </p>
                </li>
              ))}
          </ul>
        </div>
      </>
    );
  } else {
    return (
      <>
        <h1>Please Log In or Sign Up</h1>
      </>
    );
  }
}

export default PostList;
