import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

function GameEdit() {
  const { id } = useParams();
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [tagId, setTagId] = useState("");
  const [description, setDescription] = useState("");
  const [tags, setTags] = useState([]);
  const [error] = useState(null);

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const handleTagIdChange = (event) => {
    setTagId(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      title: title,
      tag_id: tagId,
      description: description
    };

    const url = `${process.env.REACT_APP_API_HOST}/games/${id}`;
    try {
      const response = await fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      if (response.ok) {
        navigate("/games");
      }
    } catch (error) {
      console.error("Error editing the game:", error);
    }
  };

  useEffect(() => {
    const fetchTags = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/tags`;
      const response = await fetch(url);
      if (response.ok) {
        const tagData = await response.json();
        setTags(tagData);
      }
    };
    fetchTags();
  }, []);

  return (
    <div className="shadow p-4 mt-4">
      <h1>Edit Game</h1>
      {error ? (
        <div>Error: {error}</div>
      ) : (
        <form onSubmit={handleSubmit}>
          <div>
            <label>Title:</label>
            <input type="text" value={title} onChange={handleTitleChange} />
          </div>
          <div>
            <label>Tag:</label>
            <select value={tagId} onChange={handleTagIdChange}>
              <option value="">Select a Tag</option>
              {tags.map((tag) => (
                <option key={tag.id} value={tag.id}>
                  {tag.tag}
                </option>
              ))}
            </select>
          </div>
          <div>
            <label>Description:</label>
            <textarea
              value={description}
              onChange={handleDescriptionChange}
            ></textarea>
          </div>
          <button>Edit</button>
        </form>
      )}
    </div>
  );
}

export default GameEdit;
