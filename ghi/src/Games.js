import React, { useEffect, useState } from "react";
import { NavLink, Link } from "react-router-dom";

const GamesList = () => {
  const [games, setGames] = useState([]);
  const [titles, setTitles] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [filteredTitles, setFilteredTitles] = useState("");
  const [searchPerformed, setSearchPerformed] = useState(false);
  const [error, setError] = useState(null);

  const fetchData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/games`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setGames(data);
      setTitles(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDeleteGame = async (id) => {
    const url = `${process.env.REACT_APP_API_HOST}/games/${id}`;
    try {
      const response = await fetch(url, {
        method: "DELETE"
      });
      if (!response.ok) {
        throw new Error("Failed to delete the game");
      }
      setGames((lastGames) => lastGames.filter((game) => game.id !== id));
    } catch (error) {
      setError(error.message);
    }
  };

  if (error) {
    return <div>Error: {error}</div>;
  }

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    const filtered = games.filter((game) =>
      game.title.toLowerCase().includes(searchTitle.toLowerCase())
    );
    setFilteredTitles(filtered);
    setSearchPerformed(true);
  };

  let appmap = searchPerformed ? filteredTitles : titles;

  return (
    <div className="shadow p-4 mt-4">
      <h1>Games</h1>
      <p>
        <NavLink to="/game-form">Create a game</NavLink>
      </p>
      <form onSubmit={handleSearchSubmit}>
        <input
          type="text"
          placeholder="Search by Title"
          value={searchTitle}
          onChange={(e) => setSearchTitle(e.target.value)}
        />
        <button type="submit">Search</button>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Title</th>
            <th>Tag</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {appmap.map((game) => {
            return (
              <tr key={game.id}>
                <td>{game.title}</td>
                <td>{game.tag}</td>
                <td>{game.description}</td>
                <td>
                  <Link to={`/game-edit/${game.id}`}>Edit</Link>
                  <button
                    onClick={() => handleDeleteGame(game.id)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
export default GamesList;
