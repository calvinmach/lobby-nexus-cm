import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

function CommunityPage() {
  const [communities, setCommunities] = useState([]);
  const [search, setSearch] = useState("");
  const { token } = useToken("");
  const handleSearchChange = (event) => setSearch(event.target.value);

  const getData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/communities`;

    const response = await fetch(url, { credentials: "include" });
    if (response.ok) {
      const data = await response.json();
      setCommunities(data);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const deleteCommunity = async (id) => {
    const detailUrl = `${process.env.REACT_APP_API_HOST}/communities/${id}`;
    const response = await fetch(detailUrl, {
      method: "DELETE",
      credentials: "include"
    });
    if (response.ok) {
      getData();
    }
  };
  if (token) {
    return (
      <>
        <NavLink to="/communities/create">Add a Community</NavLink>
        <div>
          <input
            onChange={handleSearchChange}
            value={search}
            placeholder="search by title"
            required
            type="text"
            name="search"
            id="search"
          />
          <label htmlFor="search">Search by title</label>
        </div>
        <table>
          <thead>
            <tr>
              <th>Title:</th>
              <th>Description:</th>
            </tr>
          </thead>
          <tbody>
            {communities
              .filter((community) => {
                return community.title.includes(search);
              })
              .map((community) => {
                return (
                  <tr key={community.id}>
                    <td>{community.title}</td>
                    <td>{community.description}</td>
                    <td>
                      <button onClick={() => deleteCommunity(community.id)}>
                        Delete Community?
                      </button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </>
    );
  } else {
    return (
      <>
        <h1>PLEASE SIGN UP OR SIGN IN TO CONTINUE</h1>
      </>
    );
  }
}

export default CommunityPage;
