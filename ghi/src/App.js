import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import SplashPage from "./SplashPage.js";
import Nav from "./Navigation.js";
import UserProfile from "./UserProfile";
import SignUpForm from "./Sign-up-form";
import LoginForm from "./LoginForm.js";
import CommunityPage from "./CommunitiesPage";
import PostList from "./PostList";
import PostForm from "./PostForm";
import GameForm from "./GameForm.js";
import Games from "./Games.js";
import GameEdit from "./GameEdit";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import CommunityForm from "./CommunityForm";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <>
      <div>
        <BrowserRouter basename={basename}>
          <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
            <Nav />
            <div className="container">
              <Routes>
                <Route path="" element={<SplashPage />} />
                <Route path="/sign-up" element={<SignUpForm />} />
                <Route path="/login" element={<LoginForm />} />
                <Route path="/users/profile" element={<UserProfile />} />
                <Route path="/communities/view" element={<CommunityPage />} />
                <Route path="/communities/create" element={<CommunityForm />} />
                <Route path="/games" element={<Games />} />
                <Route path="/game-form" element={<GameForm />} />
                <Route path="/game-edit/:id" element={<GameEdit />} />
                <Route path="/posts">
                  <Route index element={<PostList />} />
                  <Route path="create" element={<PostForm />} />
                </Route>
              </Routes>
            </div>
          </AuthProvider>
        </BrowserRouter>
      </div>
    </>
  );
}

export default App;
