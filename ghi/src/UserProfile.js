import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function UserProfile() {
  const [users, setUsers] = useState({});
  const { token } = useToken();
  const [setUsername] = useState("");
  const [gamertag, setGamertag] = useState("");
  const [bio, setBio] = useState("");

  const handleUsernameChange = (event) => {
    const value = event.target.value;
    setUsername(value);
  };

  const handleGamertagChange = (event) => {
    const value = event.target.value;
    setGamertag(value);
  };

  const handleBioChange = (event) => {
    const value = event.target.value;
    setBio(value);
  };

  const grabData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/token`;

    const tokenResponse = await fetch(url, {
      method: "GET",
      credentials: "include"
    });
    if (tokenResponse.ok) {
      const tokenData = await tokenResponse.json();
      setUsers(tokenData.account);
    }
  };

  useEffect(() => {
    grabData();
  }, [token]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const profileData = {};
    profileData.id = users.id;
    profileData.username = users.username;
    profileData.gamertag = gamertag;
    profileData.bio = bio;

    const updateUrl = `${process.env.REACT_APP_API_HOST}/users/profile/${users.id}`;
    const grabConfig = {
      method: "PUT",
      credentials: "include",
      body: JSON.stringify(profileData),
      headers: {
        "Content-Type": "application/json"
      }
    };

    const editResponse = await fetch(updateUrl, grabConfig);
    if (editResponse.ok) {
      setGamertag("");
      setBio("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 onChange={handleUsernameChange}>{users.username}</h1>
          <br />
          <form onSubmit={handleSubmit} id="update-profile">
            <div className="form-floating mb-3">
              <div>
                <label htmlFor="gamertag">Gamertag</label>
                <br />
                <input
                  value={gamertag}
                  onChange={handleGamertagChange}
                  placeholder="Nothing here yet"
                  required
                  type="text"
                  name="gamertag"
                  id="gamertag"
                  className="form-control"
                />
              </div>
              <br />
              <div>
                <label htmlFor="bio">Bio</label>
                <br />
                <textarea
                  value={bio}
                  onChange={handleBioChange}
                  placeholder="Sure is empty here"
                  required
                  type="box"
                  name="bio"
                  id="bio"
                  className="form-control"
                />
              </div>
              <br />
              <button className="btn btn-primary">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
